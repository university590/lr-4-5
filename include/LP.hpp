#ifndef SIMPLEX
#define SIMPLEX

#include <Eigen>

#include <vector>
#include <set>
#include <ostream>
#include <iostream>

namespace LP {

	enum class ProblemForm { canonical, general, normal };

	/**
	* Solve the linear programming problem by simplex method with an artificial basis.
	* Maximize or minimize linear manifold <c,x> with condition Ax =(<=) b.
	*
	* @param 1:			Vector 'c' of the problem <c,x> --> max
	* @param 2:			Matrix 'A' of condition Ax=b
	* @param 3:			Vector 'b' of condition Ax=b
	* @param 4:			Form of the linear programming problem
	* @param 5:			Flag to print simplex tables
	* @param 6:			Output stream
	* @return:			Solution vector
	*/
	Eigen::VectorXd solve(const Eigen::VectorXd & c,
		const Eigen::MatrixXd & A, const Eigen::VectorXd & b,
		ProblemForm form, bool verbose = false, 
		std::ostream & out = std::cout);

	/**
	* Solve the linear integer programming problem by simplex method.
	* Maximize or minimize linear manifold <c,x> with condition Ax =(<=) b,
	* where x[i], i=p..k must be integer.
	*
	* @param 1:			Vector 'c' of the problem <c,x> --> max
	* @param 2:			Matrix 'A' of condition Ax=b
	* @param 3:			Vector 'b' of condition Ax=b
	* @param 4:			Form of the linear programming problem
	* @param 5:			Set of indexes of variables which must be integer
	* @param 5:			Flag to print simplex tables
	* @param 6:			Output stream
	* @return:			Solution vector
	*/
	Eigen::VectorXd solveInteger(const Eigen::VectorXd & c,
		const Eigen::MatrixXd & A, const Eigen::VectorXd & b,
		ProblemForm form, std::set<size_t> integerSet,
		bool verbose = false, std::ostream & out = std::cout);

	class SimplexTable {
		Eigen::MatrixXd A;
		Eigen::VectorXd c;
		Eigen::MatrixXd Ab;			// Basis matrix
		Eigen::VectorXd cm;			// m coordinates of 'c' corresponding to the basis
		Eigen::VectorXd b;
		Eigen::VectorXd z;
		Eigen::VectorXd delta;
		Eigen::MatrixXd table;
		size_t _resCol;
		size_t _resRow;
		std::vector<size_t> basis;
		bool resolved;
		bool generated;

		size_t n;
		size_t m;

		void checkDimentions();
		void fillZ();
		void fillDelta();
		void fillC();
		void fillAb();
		void cleanT();

		std::ostream * out = &std::cout;

	public:
		/**
		* Swap basis vector which has number 'resolve-row' in A matrix
		* to vector which has number 'resolve-column' in A matrix. And
		* recalculate all table.
		*
		* resolve-row and resolve-column must be found by resolve method.
		*/
		void changeBasis();

		void changeBasis(size_t resRow, size_t resCol);

		/**
		* First calculation of the table.
		*/
		void generate();

		/**
		* Find resolve-row and resolve-column corresponds to minimum
		* 'delta' and 'T'.
		*
		* @return:		true if solution has fuond (all delta corresponds
		* to current basis is positive).
		*/
		bool resolve();

		/**
		* Get basis solution of Ax=b corresponds to current basis.
		*/
		Eigen::VectorXd getBasisSolution() const;

		/**
		*  Return the set of indexes of current basis columns in matrix 'A'.
		*/
		std::vector<size_t> getBasis() const;

		/**
		* Create a new simplex table without calculation values in cells.
		*
		* @param 1:			Vector 'c' of the problem <c,x> --> max
		* @param 2:			Matrix 'A' of condition Ax=b
		* @param 3:			Vector 'b' of condition Ax=b
		* @param 4:			Set of indexes of columns in matrix 'A' which is 
		*					chosen as initial basis.				
		*/
		SimplexTable(const Eigen::VectorXd & c, const Eigen::MatrixXd & A, 
			const Eigen::MatrixXd & b, const std::vector<size_t> & basis);
		SimplexTable() {}

		bool isGenerated() const;

		const Eigen::MatrixXd & rawTable() const;

		void print(size_t columnWidth = 6, char separator = '|');
		void setOutStream(std::ostream & out);
	};

}



#endif // !SIMPLEX
