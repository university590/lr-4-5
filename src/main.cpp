#include <set>
#include <iostream>
#include <fstream>

#include "LP.hpp"

using namespace std;

int main() {

	Eigen::MatrixXd A(4, 4);
	Eigen::VectorXd c(4);
	Eigen::VectorXd b(4);

	A << 1.01, 1.01, 9.45, 16,
		1. / 5, 1. / 6, 0, 0,
		0, 0, 1. / 0.3, 0,
		0, 0, 0, 1. / 0.25;

	c << 2400, 2700, 13800, 7500;
	b << 140, 21, 16, 17;

	ofstream file("tables.txt");

	auto x = LP::solve(c, A, b, LP::ProblemForm::normal, true, file);
	cout << x << endl << endl;
	file << endl << endl;

	file.close();

	Eigen::MatrixXd intA(4, 4);
	Eigen::VectorXd intC(4);
	Eigen::VectorXd intB(4);

	intA << 202, 101, 378, 320,
			12, 5, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1;
	intC << 2400, 1350, 2700, 750;
	intB << 2800000, 126000, 2400, 4250;

	/*
	// Answer: (1, 1)
	Eigen::MatrixXd intA(2, 2);
	Eigen::VectorXd intC(2);
	Eigen::VectorXd intB(2);

	intA << 2, 3,
			2, -3;
	intC << 3, 1;
	intB << 6, 3; */

	ofstream file2("tables2.txt");

	auto intX = LP::solveInteger(intC, intA, intB, LP::ProblemForm::normal,
													{ 0, 1, 2, 3 }, true, file2);
	cout << intX << endl << endl;

	file2.close();

	system("pause");
	return 0;
}
