/**
* The simplex method is implemented in accordance with
* ������ �.�., ��������� �.�. �����������: ������, �������, ������.
*  
---------------------------------------------------------------------------
|      |   c  |      |  c_0 |  c_1 |   ...   c_m |     ...     c_n |   t  |
---------------------------------------------------------------------------
|basis |      |   b  | a[0] | a[1] |   ...  a[m] |a[m+1] ...  a[n] |      |
---------------------------------------------------------------------------
| a[0] | c[0] | x[0] |     1|     0|     0|    0 |x[1,m+1] | x[1,n]|   t_0|
---------------------------------------------------------------------------
| a[1] | c[0] | x[1] |     0|     1|     0|     0|x[2,m+1] | x[2,n]|   t_0|
---------------------------------------------------------------------------
|  ... |  ... |  ... |  ... |  ... |  ... |  ... |  ...    |  ...  |  ... |
---------------------------------------------------------------------------
| a[m] | c[m] | x[m] |     0|     0|  ... |  ... |x[m,m+1] | x[m,n]|   t_m|
---------------------------------------------------------------------------
|   z  |      |      |  c_0 |  c_1 |  ... |  ... |c*x[,m+1]|c*x[,n]|      |
---------------------------------------------------------------------------
|delta |      |      |     0|     0|  ... |  ... | z-c_m+1 | z-c_n |      |
---------------------------------------------------------------------------
*/

#include "LP.hpp"

#include <iomanip>
#include <algorithm>
#include <set>
#include <iterator>
#include <cmath>
#include <vector>
#include <memory>

using namespace LP;
using namespace std;

extern const double machine_epsilon = numeric_limits<double>::epsilon();
extern const double task_epsilon = 1000 * machine_epsilon;

vector<size_t> gaussAllowableBasis(Eigen::MatrixXd & A, Eigen::VectorXd & b);
bool is_equal(double x, double y, double epsilon = task_epsilon);
bool is_less(double x, double y, double epsilon = task_epsilon);
void deleteZeros(Eigen::MatrixXd & A, double epsilon = task_epsilon);
void removeRow(Eigen::MatrixXd & matrix, size_t rowToRemove);
void removeRow(Eigen::VectorXd & matrix, size_t rowToRemove);
void columnToCanonical(Eigen::MatrixXd & expandA, size_t col, size_t row);
uint64_t fact(uint64_t n);
void addFictiveVariable(Eigen::MatrixXd & A);
vector<size_t> getRealVariables(Eigen::VectorXd x, set<size_t> toCheck);
vector<size_t> vector_difference(const vector<size_t> & v1, const vector<size_t> & v2);
vector<size_t> vector_instersection(const vector<size_t> & v1, const vector<size_t> & v2);


void simplex(Eigen::VectorXd & solution, vector<size_t> & newBasis,
	SimplexTable & outTable, const Eigen::VectorXd & c, 
	const Eigen::MatrixXd & A, const Eigen::VectorXd & b,
	vector<size_t> & basis, bool verbose, ostream & out) {

	LP::SimplexTable table(c, A, b, basis);
	table.setOutStream(out);
	table.generate();
	bool resolved = table.resolve();
	if (verbose)
		table.print(9, '|');
	while (!resolved) {
		table.changeBasis();
		resolved = table.resolve();
		if (verbose)
			table.print(9, '|');
	}

	solution = table.getBasisSolution();
	newBasis = table.getBasis();
	outTable = table;
}

Eigen::VectorXd LP::solve(const Eigen::VectorXd & c,
	const Eigen::MatrixXd & A, const Eigen::VectorXd & b,
	ProblemForm form, bool verbose,
	ostream & out) {

	Eigen::MatrixXd copy_A(A), expand_A(A), rawTable;
	Eigen::VectorXd copy_c(c), expand_c, solution;
	vector <size_t> basis, artBasis;
	LP::SimplexTable table;

	size_t m = copy_A.rows();
	size_t n = copy_A.cols();
	size_t N = copy_A.cols() + b.size();

	expand_c.resize(N);
	for (size_t i = 0; i < N; ++i)
		expand_c(i) = 0;

	addFictiveVariable(expand_A);

	for (size_t j = n; j < N; ++j)
		artBasis.push_back(j);

	switch (form) {

	case ProblemForm::canonical:

		for (size_t i = n; i < N; ++i)
			expand_c(i) = -1;

		simplex(solution, basis, table, expand_c, expand_A, b, artBasis, verbose, out);
		rawTable = table.rawTable();

		for (size_t i = n; i < N; ++i)
			if (!is_equal(solution[i], 0))
				throw runtime_error("there is no allowable solution for"
					"this LP-problem.");

		//bool isArtificalBasisDeleted = false;
		while (true) {
			// Find art basis column and resRow
			size_t resRow = -1;
			for (size_t l = 0; l < basis.size(); ++l)
				if (basis[l] >= n)
					resRow = l;
			if (resRow == -1)
				break;
			else {
				// Find resCol
				size_t resCol = -1;
				for (size_t j = 2; j < n + 2; ++j)
					if (!is_equal(rawTable(resRow, j), 0))
						resCol = j;

				// If there is no non-zero elements in resRow,
				// then this row is linearly dependent and may be deleted.
				if (resCol == -1) {
					basis.erase(basis.begin() + resRow);
					removeRow(copy_A, resRow);
					removeRow(copy_A, resRow);
					m = copy_A.rows();
				}
				else {
					table.changeBasis(resRow, resCol);
					basis[resRow] = resCol;
				}
			}

		}

		simplex(solution, basis, table, copy_c, copy_A, b, basis, verbose, out);

		return solution;

		break;

	case ProblemForm::general:
		copy_c *= -1;
		form = ProblemForm::normal;

	case ProblemForm::normal:

		Eigen::VectorXd preSolution;
		vector<size_t> basisWithoutArtBasis;

		expand_c.block(0, 0, n, 1) = copy_c;

		simplex(preSolution, basis, table, expand_c, expand_A, b, artBasis, verbose, out);

		solution.resize(n);
		for (size_t i = 0; i < n; ++i)
			solution(i) = 0;

		basisWithoutArtBasis = vector_difference(basis, artBasis);

		for (auto elem : basisWithoutArtBasis)
			solution[elem] = preSolution[elem];

		return solution;

		break;
	}

}

Eigen::VectorXd LP::solveInteger(const Eigen::VectorXd & c,
	const Eigen::MatrixXd & A, const Eigen::VectorXd & b,
	ProblemForm form, set<size_t> integerSet,
	bool verbose, ostream & out) {

	Eigen::MatrixXd copy_A(A), rawTable;
	Eigen::VectorXd copy_c(c), copy_b(b), preSolution, solution;
	vector<size_t> basis, artBasis, realVariables, notBasis, allCols, initialCols;
	SimplexTable outputTable;

	size_t m = copy_A.rows();
	size_t M = m;
	size_t n = copy_A.cols();
	size_t N = copy_A.cols();

	switch (form) {
	case ProblemForm::general:
		copy_c *= -1;
		form = ProblemForm::normal;

	case ProblemForm::normal:
		N += b.size();

		addFictiveVariable(copy_A);

		copy_c.conservativeResize(N);
		for (size_t i = n; i < N; ++i)
			copy_c(i) = 0;

		for (size_t j = n; j < N; ++j)
			artBasis.push_back(j);

		simplex(preSolution, basis, outputTable, copy_c, copy_A, copy_b, artBasis, verbose, out);

		break;

	default:
		artBasis = {};
		basis = gaussAllowableBasis(copy_A, copy_b);
		simplex(preSolution, basis, outputTable, copy_c, copy_A, copy_b, basis, verbose, out);
	}

	for (size_t j = 0; j < n; ++j)
		initialCols.push_back(j);
	for (size_t j = 0; j < N; ++j)
		allCols.push_back(j);

	realVariables = getRealVariables(preSolution, integerSet);

	while (realVariables.size() != 0) {

		rawTable = outputTable.rawTable();

		// Find 'I' such that I=argmax{preSolution[j]}, j=0..M-1, {} - fractional part.
		// And 'row' such that I=basis[row]
		size_t maxRealComponentIndex, rowOfMaxRealComponent;
		double max = 0, fractionalPart;
		for (auto rVar : realVariables) {
			fractionalPart = preSolution[rVar] - floor(preSolution[rVar]);
			if (fractionalPart > max) {
				max = fractionalPart;
				maxRealComponentIndex = rVar;
			}
		}
		rowOfMaxRealComponent = distance(basis.begin(), 
						find(basis.begin(), basis.end(), maxRealComponentIndex));

		notBasis = vector_difference(allCols, basis);

		// Expand matrixes and fill new rows
		copy_A.conservativeResize(++M, ++N);
		copy_b.conservativeResize(M);
		copy_c.conservativeResize(N);
		allCols.push_back(N - 1);

		copy_c(N - 1) = 0;
		copy_A(M - 1, N - 1) = -1;

		for (size_t i = 0; i < M - 1; ++i)
			copy_A(i, N - 1) = 0;
		for (size_t j = 0; j < N - 1; ++j)
			copy_A(M - 1, j) = 0;

		for (auto l : notBasis)
			copy_A(M - 1, l) = rawTable(rowOfMaxRealComponent, l + 2)
								- floor(rawTable(rowOfMaxRealComponent, l + 2));

		copy_b(M - 1) = preSolution(maxRealComponentIndex)
								- floor(preSolution(maxRealComponentIndex));

		// Find column with maximum delta > 0 and expand basis by this column	
		size_t newBasisElem;
		double min = numeric_limits<double>::max();
		for (auto l : notBasis) 
			if (rawTable(M, l + 2) > 0 && rawTable(M, l + 2) <= min) {
				min = rawTable(M, l + 2);
				newBasisElem = l;
			}
		basis.push_back(newBasisElem);
		
		// Solve expanded matrixes with expanded basis
		simplex(preSolution, basis, outputTable, copy_c, copy_A, copy_b, basis, verbose, out); 
		
		realVariables = getRealVariables(preSolution, integerSet);
	}
	
	solution.resize(n);
	for (size_t i = 0; i < n; ++i)
		solution(i) = 0;

	vector<size_t> resCols;
	resCols = vector_instersection(initialCols, basis);

	for (auto elem : resCols)
		solution[elem] = preSolution[elem];

	return solution;
}

/**
* Find set of basis columns which are corresponds to the allowable
* basis solution (all basis components of the solution x is positive).
* Transform matrix 'A' and vector 'b' of the system Ax=b to the form
* where basis columns are canonical (a[i]=0, i!=i0, a[i0]=1).
*
* @param 1:			Matrix 'A' of condition Ax=b
* @param 2:			Vector 'b' of condition Ax=b
* @return:			Set of indexes of the basis columns
*/
vector<size_t> gaussAllowableBasis(Eigen::MatrixXd & A, Eigen::VectorXd & b) {

	Eigen::MatrixXd expandA = A;
	expandA.conservativeResize(expandA.rows(), expandA.cols() + 1);
	expandA.block(0, expandA.cols() - 1, expandA.rows(), 1) = b.block(0, 0, expandA.rows(), 1);

	double eps = numeric_limits<double>::epsilon();

	vector<size_t> basis;
	size_t m = expandA.rows();
	size_t n = A.cols();

	// Find next basis column and lead it to canonical form 
	for (size_t i = 0, t = 0; i < m; ++i, ++t) {
		if (!is_equal(expandA(i, t), 0))
			basis.push_back(t);

		// If basis candidate has zero on row i, then:
		// 1. Try to swap row i and one of the next rows,
		//	  which hasn't zero in the same position
		// 2. If there are no rows to swap try to find new
		//    basis column among not basis columns.
		else {
			bool isFoundNextBasisElem = false;

			// Try to swap rows
			for (size_t p = i + 1; p < m; ++p)
				if (!is_equal(expandA(p, t), 0)) {
					expandA.row(i).swap(expandA.row(p));
					basis.push_back(t);
					isFoundNextBasisElem = true;
					break;
				}

			// Try to find basis column among not basis columns
			if (!isFoundNextBasisElem) {
				// Start run with column 0 because basis columns
				// have led to the canonical form and they have zero
				// in position (i,j).
				for (size_t j = 0; j < n; ++j)
					if (!is_equal(expandA(i, j), 0)) {
						basis.push_back(j);
						isFoundNextBasisElem = true;
						++t;
						break;
					}
			}
			if (!isFoundNextBasisElem)
				throw runtime_error("matrix can't be transformed into canonical form.");
		}

		// Lead new basis column to canonical form
		columnToCanonical(expandA, basis[basis.size() - 1], basis.size() - 1);
	}

	uint64_t Cnm = fact(n) / (fact(m) * fact(n - m));
	// Count of transpositions is less than combination of n to n
	// But already have one combination. Then k < Cnm - 1.
	// If all combinations is considered and all rigth parts of the system
	// have a member which is less than zero, then there is no allowable
	// basis solution.
	for (uint64_t k = 0; k < Cnm - 1; ++k) {

		// Find negative right part
		size_t rowOfNegativeRigthPart = -1;
		for (size_t i = 0; i < m; ++i)
			if (expandA(i, n) < -eps) {
				rowOfNegativeRigthPart = i;
				break;
			}

		if (rowOfNegativeRigthPart == -1)
			break;
		else {
			// Try to swap basis column to not basis column which has
			// negative value in position (rowOfNegativeRigthPart, j).
			// In new basis negative right part will be positive

			// First find not basis column with negative value in 
			// necessary position
			size_t colOfSubstisution = -1;
			for (size_t j = 0; j < n; ++j)
				if (expandA(rowOfNegativeRigthPart, j) < -eps) {
					colOfSubstisution = j;
					break;
				}
			if (colOfSubstisution == -1)
				throw runtime_error("system has no allowable basis solution.");
			else {
				// Second find basis column which needed to substitute to the
				// 'colOfSubstisution'. And find 'row' : basis['row']=column.
				size_t row;
				double min = numeric_limits<double>::max();
				double tmp;
				for (size_t p = 0; p < m; ++p)
					if (expandA(p, n) < -eps && expandA(p, colOfSubstisution) < -eps) {
						tmp = expandA(p, n) / expandA(p, colOfSubstisution);
						if (tmp < min) {
							row = p;
							min = tmp;
						}
					}
				// Swap columns
				basis[row] = colOfSubstisution;
				// Lead new column to canonical form
				columnToCanonical(expandA, colOfSubstisution, row);
			}
		}
	}

	return basis;
}

void addFictiveVariable(Eigen::MatrixXd & A) {
	size_t m = A.rows();
	size_t n = A.cols();
	size_t N = A.cols() + A.rows();
	A.conservativeResize(m, N);
	for (size_t i = 0; i < m; ++i) {
		for (size_t j = n; j < N; ++j)
			A(i, j) = 0;
		A(i, n + i) = 1;
	}
}

vector<size_t> getRealVariables(Eigen::VectorXd x, set<size_t> toCheck) {
	vector<size_t> res;
	double eps;
	uint32_t integerOrder;
	for (auto i : toCheck) {
		// Get accuracy accounting for the integer part 
		// (the mantissa is spent on the integer part).
		integerOrder = (uint32_t)ceil(log10(x[i]));
		if (integerOrder > 0 && integerOrder < 15)
			eps = pow(10, integerOrder + 1) * machine_epsilon;
		else
			eps = task_epsilon;
		if (!is_equal(x[i] - round(x[i]), 0, eps))
			res.push_back(i);
	}

	return res;
}

SimplexTable::SimplexTable(const Eigen::VectorXd & c, const Eigen::MatrixXd & A,
	const Eigen::MatrixXd & b, const vector<size_t> & basis) {

	this->A = A;
	this->c = c;
	this->b = b;
	this->basis = basis;

	checkDimentions();

	n = A.cols();
	m = A.rows();
	resolved = false;
	generated = false;
}

void SimplexTable::generate() {
	Eigen::VectorXd b_proj;			// Base decomposition of 'b'
	Eigen::MatrixXd A_proj;			// Base decomposition of 'A'

	Ab.resize(m, m);
	cm.resize(m);
	table.resize(m + 2, n + 3);

	cleanT();
	fillAb();

	b_proj = Ab.inverse() * b;
	A_proj = Ab.inverse() * A;

	table.block(0, 2, m, n) = A_proj;
	table.block(0, 1, m, 1) = b_proj;

	fillC();
	fillZ();
	fillDelta();

	generated = true;
}

bool SimplexTable::resolve() {
	vector<size_t> resColsVector;
	double minDelta = numeric_limits<double>().max();
	double minT = numeric_limits<double>().max();
	bool hasNegativeDelta = false;

	for (size_t j = 2; j < n + 2; ++j)
		if (is_less(table(m + 1, j), 0)) {
			hasNegativeDelta = true;
			if (table(m + 1, j) < minDelta) {
				minDelta = table(m + 1, j);
				resColsVector.clear();
				resColsVector.push_back(j);
			}
			else if (table(m + 1, j) == minDelta)
				resColsVector.push_back(j);
		}

	for (auto resColCandidate : resColsVector)
		for (size_t i = 0; i < m; ++i)
			if (table(i, resColCandidate) > 0) {
				double T = table(i, 1) / table(i, resColCandidate);
				if (T < minT) {
					minT = T;
					_resCol = resColCandidate;
					_resRow = i;
				}
			}

	cleanT();
	table(_resRow, n + 2) = minT;
	deleteZeros(table);
	resolved = true;
	return !hasNegativeDelta;
}

void SimplexTable::changeBasis() {
	if (!resolved)
		throw runtime_error("table need to be resolved before changing basis.");
	else {
		basis[_resRow] = _resCol - 2;

		fillC();
		changeBasis(_resRow, _resCol);
		fillZ();
		fillDelta();

		resolved = false;
	}
}

void SimplexTable::changeBasis(size_t resRow, size_t resCol) {
	Eigen::VectorXd resColCopy(table.col(resCol));
	for (size_t i = 0; i < m; ++i) {
		if (i == resRow)
			continue;

		for (size_t j = 1; j < 2 + n; ++j)
			table(i, j) = table(i, j) -
				table(resRow, j) * resColCopy(i) / resColCopy(resRow);
	}

	for (size_t j = 1; j < 2 + n; ++j)
		table(resRow, j) = table(resRow, j) / resColCopy(resRow);
}

void SimplexTable::fillZ() {
	table(m, 0) = 0;
	for (size_t j = 1; j < n + 2; ++j)
		table(m, j) = table.col(0).segment(0, m).transpose() *
		table.col(j).segment(0, m);
}

void SimplexTable::fillDelta() {
	table(m + 1, 0) = 0;
	table(m + 1, 1) = 0;
	for (size_t j = 2; j < n + 2; ++j)
		table(m + 1, j) = table(m, j) - c(j - 2);
}

void SimplexTable::fillC() {
	for (size_t i = 0; i < m; ++i)
		cm(i) = c(basis[i]);

	table.block(0, 0, m, 1) = cm;
}

void SimplexTable::fillAb() {
	for (size_t i = 0; i < m; ++i)
		Ab.col(i) = A.col(basis[i]);
}

void SimplexTable::cleanT() {
	for (size_t i = 0; i < m + 2; ++i)
		table(i, n + 2) = numeric_limits<double>().quiet_NaN();
}

Eigen::VectorXd SimplexTable::getBasisSolution() const {
	Eigen::VectorXd res(n);
	res.fill(0);
	for (size_t i = 0; i < m; ++i)
		res[basis[i]] = table(i, 1);
	return res;
}

vector<size_t> SimplexTable::getBasis() const {
	return basis;
}

void SimplexTable::checkDimentions() {
	if (c.size() != A.cols() ||
		basis.size() > A.rows() ||
		b.size() != A.rows() ||
		A.rows() >= A.cols()
		)
		throw invalid_argument("invalid dimentions.");
}


const Eigen::MatrixXd & SimplexTable::rawTable() const {
	return table;
}

bool SimplexTable::isGenerated() const {
	return generated;
}

void SimplexTable::print(size_t columnWidth, char separator) {
	*out << setprecision(3);
	*out << /*string((columnWidth + 1) * (n + 4), '-') <<*/ endl;
	*out << setw(columnWidth) << "" << separator <<
		setw(columnWidth) << "c" << separator <<
		setw(columnWidth) << "" << separator;
	for (size_t j = 0; j < n; ++j)
		*out << setw(columnWidth) << c(j) << separator;
	*out << setw(columnWidth) << "t" << separator;
	*out << endl /*<< string((columnWidth + 1) * (n + 4), '-') << endl*/;

	*out << setw(columnWidth) << left << "basis" << separator << right <<
		setw(columnWidth) << "" << separator <<
		setw(columnWidth) << left << "b" << separator << right;
	for (size_t j = 0; j < n; ++j) {
		string AElemName = "a[" + to_string(j) + "]";
		*out << setw(columnWidth) << left << AElemName << separator << right;
	}
	*out << setw(columnWidth) << "" << separator;
	*out << endl /*<< string((columnWidth + 1) * (n + 4), '-') << endl*/;

	for (size_t i = 0; i < m; ++i) {
		string basisElemName = "a[" + to_string(basis[i]) + "]";
		*out << setw(columnWidth) << left << basisElemName << separator << right;
		for (size_t j = 0; j < n + 3; ++j) {
			*out << setw(columnWidth) << table(i, j) << separator;
		}
		*out << endl /*<< string((columnWidth + 1) * (n + 4), '-') << endl*/;
	}

	*out << setw(columnWidth) << left << "z" << separator << right;
	for (size_t j = 0; j < n + 2; ++j) {
		*out << setw(columnWidth) << table(m, j) << separator;
	}
	*out << setw(columnWidth) << "" << separator;
	*out << endl /*<< string((columnWidth + 1) * (n + 4), '-') << endl*/;

	*out << setw(columnWidth) << left << "delta" << separator << right;
	for (size_t j = 0; j < n + 2; ++j) {
		*out << setw(columnWidth) << table(m + 1, j) << separator;
	}
	*out << setw(columnWidth) << "" << separator;
	*out << endl /*<< string((columnWidth + 1) * (n + 4), '-') << endl*/;
	*out << endl;
}

void SimplexTable::setOutStream(ostream & out) {
	this->out = &out;
}


bool is_equal(double x, double y, double epsilon) {
	return (x < y + epsilon) && (x > y - epsilon);
}

bool is_less(double x, double y, double epsilon) {
	return (x < y - epsilon);
}

uint64_t fact(uint64_t n) {
	uint64_t ret = 1;
	for (size_t i = 1; i <= n; ++i)
		ret *= i;
	return ret;
}

void columnToCanonical(Eigen::MatrixXd & expandA, size_t col, size_t row) {
	size_t m = expandA.rows();
	double k;

	if (is_less(expandA(row, col), 0))
		expandA.row(row) *= -1;

	expandA.row(row) /= expandA(row, col);

	for (size_t i = 0; i < m; ++i) {
		if (i == row)
			continue;
		if (!is_equal(expandA(i, col), 0)) {
			k = -expandA(i, col) / expandA(row, col);
			expandA.row(i) += k * expandA.row(row);
		}
	}
}

void removeRow(Eigen::MatrixXd & matrix, size_t rowToRemove) {
	size_t numRows = matrix.rows() - 1;
	size_t numCols = matrix.cols();

	if (rowToRemove < numRows)
		matrix.block(rowToRemove, 0, numRows - rowToRemove, numCols) = matrix.block(rowToRemove + 1, 0, numRows - rowToRemove, numCols);

	matrix.conservativeResize(numRows, numCols);
}

void removeRow(Eigen::VectorXd & matrix, size_t rowToRemove) {
	size_t numRows = matrix.rows() - 1;
	size_t numCols = matrix.cols();

	if (rowToRemove < numRows)
		matrix.block(rowToRemove, 0, numRows - rowToRemove, numCols) = matrix.block(rowToRemove + 1, 0, numRows - rowToRemove, numCols);

	matrix.conservativeResize(numRows, numCols);
}

void deleteZeros(Eigen::MatrixXd & A, double epsilon) {
	for (size_t i = 0; i < A.rows(); ++i)
		for (size_t j = 0; j < A.cols(); ++j)
			if (is_equal(A(i, j), 0))
				A(i, j) = 0;
}

vector<size_t> vector_difference(const vector<size_t> & v1, const vector<size_t> & v2) {
	vector<size_t> sortedV1(v1);
	vector<size_t> sortedV2(v2);
	vector<size_t> res;

	sort(sortedV1.begin(), sortedV1.end());
	sort(sortedV2.begin(), sortedV2.end());

	set_difference(sortedV1.begin(), sortedV1.end(), sortedV2.begin(),
		sortedV2.end(), back_inserter(res));

	return res;
}

vector<size_t> vector_instersection(const vector<size_t> & v1, const vector<size_t> & v2) {
	vector<size_t> sortedV1(v1);
	vector<size_t> sortedV2(v2);
	vector<size_t> res;

	sort(sortedV1.begin(), sortedV1.end());
	sort(sortedV2.begin(), sortedV2.end());

	set_intersection(sortedV1.begin(), sortedV1.end(), sortedV2.begin(),
		sortedV2.end(), back_inserter(res));

	return res;
}

